from django.urls import path
from .views import formulario_view, mascota_list

urlpatterns = [
    path('registro',formulario_view, name = 'formulario_crear'),
    path('listar',mascota_list, name = 'formulario_listar'),
    #path('listar',login_required(mascota_list), name = 'mascota_listar'),

]