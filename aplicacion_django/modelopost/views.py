from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import FormularioForms
from .models import Formulario

# Create your views here.
def formulario_view(request):
    if request.method == 'POST':
        form = FormularioForms(request.POST)
        if form.is_valid():
            form.save()
        return render(request, 'rename.html')
    else:
        form = FormularioForms()
    return render(request, 'formulario.html', {'form':form})



def mascota_list(request):
    mascota = Formulario.objects.all() #mascota: es la variable que contiene los objects
    contexto = {'mascotas':mascota}  #'mascota' es la clave para acceder al diccionario
    return render(request, 'listar.html', contexto)